//
//  main.m
//
//  Reads a formated file called Person.dat, populates an array of
//  Person objects, prints out all of the people's names sorted by
//  name, and prints the average age of all of the people.
//

#import <Foundation/Foundation.h>

// The structure of a Person object.
@interface Person: NSObject
{
    NSString *name;
    NSInteger age;
}
    @property() NSString *name;
    @property() NSInteger age;
    -(void) display;
    - (NSComparisonResult)compare:(Person *)otherPerson;
@end


// Implement a Person object.
@implementation Person

    // Synthesize the name to the Person object.
    @synthesize name;
    // Synthesize the age to the Person object.
    @synthesize age;

    // Display the name of the person.
    -(void) display {
        NSLog(@"%@",name);
    }

    // Compare the Person objects based on name.
    - (NSComparisonResult) compare: (Person *) otherPerson {
        return [self.name compare: otherPerson.name];
    }
@end


int main(int argc, const char * argv[]) {
    @autoreleasepool {
        // Variable to keep track of the total age of every person in the datatbase.
        NSInteger totalAge = 0;
        // Allocate space for an array to hold Person objects.
        NSMutableArray *personArray = [[NSMutableArray alloc] init];
        
        // Turn the contents of the file into a string.
        NSString *fileContent = [NSString stringWithContentsOfFile:@"/Users/Joel/Documents/Xcode/Person_ObjC/Person_ObjC/Person.dat" encoding:NSUTF8StringEncoding error:nil];
        
        // Split the string of file contents at every new line character.
        NSArray *fileLines = [fileContent componentsSeparatedByString:@"\n"];
        
        // For every line in the file, add it's contents to a Person object,
        //  and add to the array.
        for (int i = 0; i < [fileLines count] - 1; i++)
        {
            // Split the line, using a comma as the delimeter.
            NSArray *items = [[fileLines objectAtIndex:i] componentsSeparatedByString:@","];
            // Create a person.
            Person *person = [[Person alloc] init];
            // Update its attributes.
            person.name = [items objectAtIndex:0];
            person.age = [[items objectAtIndex:1] integerValue];
            // Add person to the array.
            [personArray addObject:person];
        }

        // Create an array that sorted the personArray using the Person comparator.
        NSArray *sortedArray = [personArray sortedArrayUsingSelector:@selector(compare:)];
        
        Person *currentPerson;
        // For every person in the sorted array, display and add its age to the total age.
        for (int i = 0; i < [sortedArray count]; i++)
        {
            currentPerson = [sortedArray objectAtIndex:i];
            totalAge += currentPerson.age;
            [currentPerson display];
        }
        
        // Print out the average age by dividing the total age by the number of people.
        NSLog(@"Average Age: %li",((long)totalAge) / [sortedArray count]);
    } // autoreleasepool
} // main
